import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { AppService } from './app.service';

interface Currency {
  currency: string;
  ask: number;
  bid: number;
  code: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  options: Array<any> = [];
  selectedCurrency: Currency | null = null;
  checked: boolean = false;
  value1: number = 1000;
  value2: number = 1000;
  chenge: string = 'value2';

  constructor(
    private appService: AppService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.getExchangeRates();
  }

  getExchangeRates() {
    this.appService.exchangeRates().subscribe({
      next: res => {
        this.options = res.map((cur: Currency) => { return { ...cur, label: `${cur.currency} (${cur.code})` } });
        this.selectedCurrency = this.options[0];
        this.calculate();
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Dane zostały zaczytane' });
      },
      error: error => {
        console.error('BŁAD POŁĄCZNIA Z NBP', error);
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Wystąpił problem z wczytaniem danych' });
      },
    });
  }

  calculate() {
    const coefficient = (this.checked ? this.selectedCurrency?.ask : this.selectedCurrency?.bid) ?? 0;
    if (this.chenge === 'value1')
      this.value1 = this.checked ? Math.floor(this.value2 / coefficient * 100) / 100 : Math.floor(this.value2 * coefficient * 100) / 100;
    else
      this.value2 = this.checked ? Math.floor(this.value1 * coefficient * 100) / 100 : Math.floor(this.value1 / coefficient * 100) / 100;
  }

  changesValue1() {
    this.chenge = 'value2';
    this.calculate();
  }

  changesValue2() {
    this.chenge = 'value1';
    this.calculate();
  }
}
