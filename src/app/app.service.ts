import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private httpClient: HttpClient) { }

  exchangeRates(): Observable<any> {
    return this.httpClient.get(`http://api.nbp.pl/api/exchangerates/tables/c/`)
      .pipe(map((res: any) => res?.['0']?.rates));
  }
}
